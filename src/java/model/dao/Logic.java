/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import blockchain.BlockChainApI;
import blockchain.Transaction;
import database.DbConnect;
import encryption.Aes;
import encryption.Hash;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.pojo.Body;
import model.pojo.Certificate;
import model.pojo.Holder;

/**
 *
 * @author mujistapha
 */
public class Logic {
    DbConnect db = new DbConnect();
    Hash hash = new Hash();
    Aes aes = new Aes();
    
    Connection con = null;
    Statement stmt = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    
    BlockChainApI blockChainApI = new BlockChainApI();
    
    public void registerBody(Body body) throws SQLException, Exception{
        int id = 0;
        con = db.getConnection();
        String query = "insert into Body values (ID, '"+body.getName()+"', '"+body.getLicense()+"', "
                + "'"+body.getAddress()+"', '"+body.getContact()+"', '"+body.getUsername()+"', "
                + "'"+Hash.getSaltedHash(body.getPassword()) +"' )";
        pstmt = con.prepareStatement(query);    
        pstmt.executeUpdate();
        con.close();
    }
    
    public boolean ifUserExist(String name, String username) throws SQLException{
        boolean body = false;
        con = db.getConnection();
        String query = "select * from Body where username like '"+username+"' || Name like '"+name+"' ";
        pstmt = con.prepareStatement(query);
        rs = pstmt.executeQuery();
        while(rs.next()){
            body = true;
        }
        return body;
    }
    
    public Body loggedBody(String username) throws SQLException{
        Body body = new Body();
        int id = 0;
        con = db.getConnection();
        String query = "select * from Body where username like '"+username+"' ";
        pstmt = con.prepareStatement(query);
        rs = pstmt.executeQuery();
        while(rs.next()){
            body = new Body(rs.getInt("ID"), rs.getString("name"), rs.getString("LicenceID"), 
                    rs.getString("Address"), rs.getString("Contact"));
        }
        return body;
    }
    
    public boolean login(String username, String password){
        Boolean result = false;
        try {
            con = db.getConnection();
            String query = "select * from Body where username like '"+username+"'";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            String userType = null;
            while(rs.next()){
                String stored = rs.getString("password");
                if(Hash.check(password, stored)){
                    result = true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
    
    public List<Transaction> verify(int id){
        return blockChainApI.getCertificates(id);
    }
    
    public List<Certificate> allCerts(Body body){
        ArrayList<Certificate> certificates = new ArrayList<>();
        try {
            con = db.getConnection();
            String query =  "select * from Certificate where Certificate_Body = "+body.getId()+" ";
            pstmt=con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while(rs.next()){
                Certificate certificate = new Certificate(  rs.getInt("ID"), 
                                                            rs.getInt("Certificate_ID"), 
                                                            rs.getDate("Date").toString(), 
                                                            rs.getString("AwardDate"), 
                                                            rs.getString("Hash"), 
                                                            rs.getString("Status"));
                certificates.add(certificate);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return certificates;
    }
    
    public List<Certificate> searchCerts(Body body, String id){
        ArrayList<Certificate> certificates = new ArrayList<>();
        try {
            con = db.getConnection();
            String query =  "select * from Certificate where Certificate_Body = "+body.getId()+" and Certificate_ID = "+id+" ";
            pstmt=con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while(rs.next()){
                Certificate certificate = new Certificate(  rs.getInt("ID"), 
                                                            rs.getInt("Certificate_ID"), 
                                                            rs.getDate("Date").toString(), 
                                                            rs.getString("AwardDate").toString(), 
                                                            rs.getString("Hash"), 
                                                            rs.getString("Status"));
                certificates.add(certificate);
            }
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return certificates;
    }
    
   public void add(Holder holder, Certificate certificate){
        try {
            con = db.getConnection();
            String newHolder = "insert into Holder values(ID, '"+holder.getFullname()+"', '"
                    +holder.getDob()+"', '"+holder.getNumber()+"', '"+holder.getEmail()+"' ) ";
            stmt = con.prepareStatement(newHolder, com.mysql.jdbc.Statement.RETURN_GENERATED_KEYS);
            int s =  stmt.executeUpdate(newHolder,Statement.RETURN_GENERATED_KEYS);
            rs = stmt.getGeneratedKeys();
            int id = 0;
            while(rs.next()){
                id = rs.getInt(1);
            }
            String newCert = "insert into Certificate values (Id,  " +certificate.getCertificateId()+", '"
                     
                                                                    +Aes.encrypt(certificate.getTitle())+"', "
                                                                    +certificate.getBody()+", now(), '"
                                                                    +certificate.getAwardDate()+"', "
                                                                    +id+", '"
                                                                    +certificate.getHash()+"', '"
                                                                    +certificate.getStatus()+"', 'not set' ) ";
            pstmt = con.prepareStatement(newCert);    
            pstmt.executeUpdate();
            con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
   
   public boolean updatePassword(String username, String newPassword){
       boolean result = false; 
       try {
            con = db.getConnection();
            String query = "update body set password = '"+Hash.getSaltedHash(newPassword)+"' where username = '"+username+"' ";
            pstmt = con.prepareStatement(query);
            pstmt.executeUpdate();
            if(pstmt.executeUpdate() > 0 ){
                result = true;
            }} catch (SQLException ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Logic.class.getName()).log(Level.SEVERE, null, ex);
        }
      return result;
   }
    
}
