/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.pojo;

/**
 *
 * @author mujistapha
 */
public class Holder {
    
    private int id;
    private String fullname;
    private String dob;
    private String number;
    private String email;

    public Holder(int id, String fullname, String dob, String number, String email) {
        this.id = id;
        this.fullname = fullname;
        this.dob = dob;
        this.number = number;
        this.email = email;
    }

    public Holder(String fullname, String dob, String number, String email) {
        this.fullname = fullname;
        this.dob = dob;
        this.number = number;
        this.email = email;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}
