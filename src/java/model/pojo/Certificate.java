/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.pojo;

/**
 *
 * @author mujistapha
 */
public class Certificate {
    
    private int ID;
    private int certificateId;
    private String title;
    private int body;
    private String date;
    private String awardDate;
    private int holder;
    private String hash;
    private String status;

    public Certificate(int ID, int certificateId, String title, int body, String date, String awardDate, int holder, String hash,  String status) {
        this.ID = ID;
        this.certificateId = certificateId;
        this.title = title;
        this.body = body;
        this.date = date;
        this.awardDate = awardDate;
        this.holder = holder;
        this.hash = hash;
        this.status = status;
    }

    public Certificate(int ID, int certificateId, String date, String awardDate, String hash, String status) {
        this.ID = ID;
        this.certificateId = certificateId;
        this.date = date;
        this.awardDate = awardDate;
        this.hash = hash;
        this.status = status;
    }

    public Certificate(int certificateId, String hash, String title, int body,  String awardDate,   String status) {
        this.certificateId = certificateId;
        this.hash = hash;
        this.title = title;
        this.body = body;
        this.awardDate = awardDate;
        this.status = status;
    }
    
    

    
    
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getCertificateId() {
        return certificateId;
    }

    public void setCertificateId(int certificateId) {
        this.certificateId = certificateId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAwardDate() {
        return awardDate;
    }

    public void setAwardDate(String awardDate) {
        this.awardDate = awardDate;
    }

    public int getHolder() {
        return holder;
    }

    public void setHolder(int holder) {
        this.holder = holder;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    
    
    
    
    
}
