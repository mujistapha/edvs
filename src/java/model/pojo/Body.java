/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.pojo;

/**
 *
 * @author mujistapha
 */
public class Body {
    
    private int id;
    private String name;
    private String license;
    private String address;
    private String contact;
    private String username;
    private String password;

    public Body() {
    }

    public Body(String name, String license, String address, String contact, String username, String password) {
        this.name = name;
        this.license = license;
        this.address = address;
        this.contact = contact;
        this.username = username;
        this.password = password;
    }
    
    public Body(int id, String name, String license, String address, String contact) {
        this.id = id;
        this.name = name;
        this.license = license;
        this.address = address;
        this.contact = contact;
    }

    public Body(int id, String name, String license, String address, String contact, String username, String password) {
        this.id = id;
        this.name = name;
        this.license = license;
        this.address = address;
        this.contact = contact;
        this.username = username;
        this.password = password;
    }

    public Body(int id) {
        this.id = id;
    }
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
