/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.rest;

import blockchain.BlockChainApI;
import blockchain.Transaction;
import com.google.gson.Gson;
import java.util.List;
import model.dao.Logic;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author abdulrahmanmusa
 */
@RestController
public class VerifyRestController {
    Logic logic = new Logic();
    BlockChainApI blockChainApI = new BlockChainApI();

    @ResponseBody
    @RequestMapping(value = "/verify/{id}", method = RequestMethod.GET)
    public String test(@PathVariable("id") int id) {
        List<Transaction> list = logic.verify(id);
        Gson gson = new Gson();
        return gson.toJson(list);
    }

}


