/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import blockchain.BlockChainApI;
import blockchain.Transaction;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import model.dao.Logic;
import model.pojo.Body;
import model.pojo.Certificate;
import model.pojo.Holder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author mujistapha
 */
@Controller
public class MainController {
    Logic logic = new Logic();
    BlockChainApI blockChainApI = new BlockChainApI();
    
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model, HttpServletRequest request) throws Exception {   
        System.out.println("+++++++++++");
        return "index";
    }
    
    @RequestMapping(value = "/certdetail", method = RequestMethod.GET)
    public String certdetail(Model model, HttpServletRequest request) throws Exception { 
        List<Transaction> list = logic.verify(Integer.valueOf(request.getParameter("val")));
        model.addAttribute("certDetails", list);
        return "certdetail";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(Model model, HttpServletRequest request) throws SQLException, Exception {
        String username = request.getParameter("Username");
        String password = request.getParameter("Password");
        if(logic.login(username, password)){
            Body body = logic.loggedBody(username);
            request.getSession().setAttribute("username", username);
            request.getSession().setAttribute("id", body.getId());
            return home(model, request);
        } else {
            request.getSession().setAttribute("errorMessage", "User does not exist");
            return "redirect:index.htm";
        }
        
        
    }
    
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model, HttpServletRequest request) throws Exception {   
        return "registration";
    }
    
    
            
    @RequestMapping(value = "/resetPassword", method = RequestMethod.GET)
    public String resetPassword(Model model, HttpServletRequest request) throws Exception {   
        String r = null;
        logic.updatePassword(request.getSession().getAttribute("username").toString(), request.getParameter("new"));
        if(logic.login(request.getSession().getAttribute("username").toString(), request.getParameter("new"))){          
            Body body = new Body(Integer.valueOf(request.getSession().getAttribute("id").toString()));
            List<Certificate> list = logic.allCerts(body);
            model.addAttribute("ListOfCert", list);
            return "index";           
        } else { 
            request.getSession().setAttribute("errorMessage", "Wrong password");
            return "redirect:settings.htm";   
        }
    }
            
    @RequestMapping(value = "/forgotpassword", method = RequestMethod.GET)
    public String forgotpassword(Model model, HttpServletRequest request) throws Exception {
        return "forgotpassword";
    }
    
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model, HttpServletRequest request) throws Exception {
        Body body = new Body(Integer.valueOf(request.getSession().getAttribute("id").toString()));
        List<Certificate> list = logic.allCerts(body);
     
        model.addAttribute("ListOfCert", list);
        return "home";
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model, HttpServletRequest request) throws Exception {
        String name = request.getParameter("name"); 
        String license = request.getParameter("license"); 
        String address = request.getParameter("address"); 
        String contact = request.getParameter("number"); 
        String username = request.getParameter("username"); 
        String password = request.getParameter("password");
        Body body = new Body(1,name, license, address, contact, username, password);
        if(!logic.ifUserExist(name, username)){
            String val = "";
            logic.registerBody(body);
            if(logic.login(username, password)){
                Body bod = logic.loggedBody(username);
                request.getSession().setAttribute("username", username);
                request.getSession().setAttribute("id", bod.getId());
                List<Certificate> list = logic.allCerts(body);
                model.addAttribute("ListOfCert", list);
                val = "home";
            }
            return val;
        } else {
            request.getSession().setAttribute("errorMessage", "User with same username or body exist");
            return "redirect:registration.htm";
        }
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model, HttpServletRequest request) throws Exception {
        return "add";
    }
    
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(Model model, HttpServletRequest request) throws Exception {
        Body body = new Body(Integer.valueOf(request.getSession().getAttribute("id").toString()));
        String id = request.getParameter("searchid");
        
        List<Certificate> lst = logic.searchCerts(body, id);
                model.addAttribute("ListOfCert", lst);
        return "home";
    }
    
    @RequestMapping(value = "/settings", method = RequestMethod.GET)
    public String settings(Model model, HttpServletRequest request) throws Exception {
        return "settings";
    }
    
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model, HttpServletRequest request) throws Exception {
        request.getSession().setAttribute("id", null);
        return "index";
    }
    
    @RequestMapping(value = "/addCert", method = RequestMethod.GET)
    public String addcert(Model model, HttpServletRequest request) throws Exception {
        
        String fullname = request.getParameter("fullname");
        String dob = request.getParameter("dob");
        String number = request.getParameter("mobileno");
        String email = request.getParameter("email");
        
        String title = request.getParameter("title");
        int certificateId =  Integer.valueOf(request.getParameter("certId"));
        int body = Integer.valueOf(request.getSession().getAttribute("id").toString());
        String awardDate = request.getParameter("dateawarded");
        String status = request.getParameter("comment");
        
        
        
        Transaction transaction = new Transaction(body, fullname, certificateId, status);
        
        
        
        String hash = blockChainApI.addToBlockchain(transaction);
        
        Holder holder = new Holder(fullname, dob, number, email);
        Certificate certificate = new Certificate(certificateId, hash, title, body, awardDate, status);
        logic.add(holder, certificate);
        
        
        
        Body certbody = new Body(Integer.valueOf(request.getSession().getAttribute("id").toString()));
        List<Certificate> list = logic.allCerts(certbody);
        model.addAttribute("ListOfCert", list);
        return "home";
    }
    
    @RequestMapping(value = "/updatPassword", method = RequestMethod.GET)
    public String updatPassword(Model model, HttpServletRequest request) throws Exception {
        String r = null;
        logic.updatePassword(request.getSession().getAttribute("username").toString(), request.getParameter("new"));
        if(logic.login(request.getSession().getAttribute("username").toString(), request.getParameter("new"))){          
            Body body = new Body(Integer.valueOf(request.getSession().getAttribute("id").toString()));
            List<Certificate> list = logic.allCerts(body);
            model.addAttribute("ListOfCert", list);
            return "index";           
        } else { 
            request.getSession().setAttribute("errorMessage", "Wrong password");
            return "redirect:settings.htm";   
        }
    }
   
    
}
