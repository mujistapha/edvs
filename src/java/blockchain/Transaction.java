package blockchain;


public class Transaction
{
    private int awardingBody;
    private String holder;
    private int certificateID;
    private String comment;

    public Transaction() {
    }
    
    public Transaction(int awardingBody, String holder, int certificateID, String comment) {
        this.awardingBody = awardingBody;
        this.holder = holder;
        this.certificateID = certificateID;
        this.comment = comment;
    }
    
    public int getawardingBody() {
        return awardingBody;
    }

    public void setawardingBody(int awardingBody) {
        this.awardingBody = awardingBody;
    }

    public String getholder() {
        return holder;
    }

    public void setholder(String holder) {
        this.holder = holder;
    }

    public int getcertificateID() {
        return certificateID;
    }

    public void setcertificateID(int certificateID) {
        this.certificateID = certificateID;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Transaction that = (Transaction) o;

        if (Double.compare(that.certificateID, certificateID) != 0) return false;
        if (awardingBody != that.awardingBody) return false;
        if (!holder.equals(that.holder)) return false;
        return comment.equals(that.comment);

    }

    @Override
    public int hashCode() {
        int result;
        int temp;
        result = awardingBody;
        result = 31 * result + holder.hashCode();
        temp = certificateID;
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + comment.hashCode();
        return result;
    }
}
