package blockchain;



import com.sun.net.ssl.internal.www.protocol.https.HttpsURLConnectionOldImpl;
import encryption.Aes;
import sun.net.www.protocol.http.HttpURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.HttpsURLConnection;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONException;



public class BlockChainApI {
    Aes aes = new Aes();

    private final String server = "http://localhost:3001/";
    private final String blockUrl = "http://localhost:3001/blocks";
    
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private JSONArray getData(String urlString) throws Exception {
        final ArrayList<JSONArray> lst = new ArrayList<>();        
        
        URL obj = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        // optional default is GET
        con.setRequestMethod("GET");
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                JSONArray jsonArray = new JSONArray(inputLine);
                lst.add(jsonArray);
            }
        }
        return lst.get(0);
    }

    //From here 
    
    private String addBlock(Transaction transaction) throws Exception {
        JSONObject jObject = new JSONObject();
        String url = server + "mineBlock";
        JSONObject result = new JSONObject();
        try {
            jObject.put("awardingBody", Aes.encrypt(String.valueOf(transaction.getawardingBody())));
            jObject.put("holder", Aes.encrypt(transaction.getholder()));
            jObject.put("comment", Aes.encrypt(transaction.getComment()));
            jObject.put("certificate", Aes.encrypt(String.valueOf(transaction.getcertificateID())));

            
            result.put("data", jObject);

            
            
        } catch (Exception ex) {
            throw ex;
        }
        return post(url, result.toString());

    }
    
    private ArrayList<Transaction> getTransactions() {
        ArrayList<Transaction> transactions = new ArrayList<>();
        ArrayList<Block> blocks = getBlocks();
        Transaction transaction = null;
        
        try {
            for (Block block : blocks) {
                if (block.getIndex() != 0) {

                    JSONObject object = block.getData();
                    transaction = new Transaction();
                    
                    Transaction t = new Transaction(    Integer.valueOf(Aes.decrypt(object.getString("awardingBody"))), 
                                                        Aes.decrypt(object.getString("holder")), 
                                                        Double.valueOf(Aes.decrypt(object.getString("certificate"))).intValue(), 
                                                        Aes.decrypt(object.getString("comment")));
                    transactions.add(t);
                    
                }
            }
        } catch (JSONException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(BlockChainApI.class.getName()).log(Level.SEVERE, null, ex);
        }

        return transactions;
    }
    
    //To here 
    
    //from here 
    
//    private void addBlock(Transaction transaction) throws Exception {
//        JSONObject jObject = new JSONObject();
//        String url = server + "mineBlock";
//
//        try {
//            jObject.put("awardingBody", transaction.getawardingBody());
//            jObject.put("holder", transaction.getholder());
//            jObject.put("comment", transaction.getComment());
//            jObject.put("certificate", transaction.getcertificateID());
//
//            JSONObject result = new JSONObject();
//            result.put("data", jObject);
//
//            post(url, result.toString());
//            
//        } catch (Exception ex) {
//            throw ex;
//        }
//
//    }
    
    
//    private ArrayList<Transaction> getTransactions() {
//        ArrayList<Transaction> transactions = new ArrayList<>();
//        ArrayList<Block> blocks = getBlocks();
//        Transaction transaction = null;
//        
//        try {
//            for (Block block : blocks) {
//                if (block.getIndex() != 0) {
//
//                    JSONObject object = block.getData();
//                    transaction = new Transaction();
//                    transaction.setcertificateID(object.getInt("certificate"));
//                    transaction.setComment(object.getString("comment"));
//                    transaction.setholder(object.getString("holder"));
//                    System.out.println("Malam " + object.getString("holder"));
//                    transaction.setholder(String.valueOf(object.getInt("awardingBody")));
//                    transactions.add(transaction);
//                }
//            }
//        } catch (JSONException ex) {
//            System.out.println(ex.getMessage());
//        }
//
//        return transactions;
//    }
    
    //To here

    private ArrayList<Block> getBlocks() {
        final ArrayList<Block> blocks = new ArrayList<>();

        try {
            JSONArray array = getData(blockUrl);
            Block block = null;
            for (int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);
                block = new Block();
                if (obj.getInt("index") != 0) {
                    block.setData(obj.getJSONObject("data"));
                    block.setHash(obj.getString("hash"));
                    block.setIndex(obj.getInt("index"));
                    block.setTimetamp(String.valueOf(obj.getInt("timestamp")));
                    block.setPreviousHash(obj.getString("previousHash"));
                    blocks.add(block);
                }
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return blocks;
    }

    public ArrayList<Transaction> getCertificates(int certificateID) {
        ArrayList<Transaction> transactions = new ArrayList<>();
        for (Transaction transaction : getTransactions()) {
            System.out.println(transaction.getcertificateID() +" &&& "+ certificateID);
            if(transaction.getcertificateID() == certificateID){
                transactions.add(transaction);
            }
            
        }
        return transactions;
    }

    public String addToBlockchain(Transaction transaction) throws Exception {
        return addBlock(transaction);
    }

    public String post(String url, String json) throws IOException, JSONException {
       
      
        
        RequestBody body = RequestBody.create(JSON, json);
        Request request = new Request.Builder()
            .url(url)
            .post(body)
            .build();
        
        OkHttpClient client = new OkHttpClient();
        Response response = client.newCall(request).execute();
        JSONObject objectFromString = new JSONObject(response.body().string());
        return objectFromString.optString("result");
    }
    
    
}
