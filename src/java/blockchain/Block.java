package blockchain;


import org.json.JSONObject;

public class Block
{
    private int index;
    private String timetamp;
    private JSONObject data;
    private String hash;
    private String previousHash;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getTimetamp() {
        return timetamp;
    }

    public void setTimetamp(String timetamp) {
        this.timetamp = timetamp;
    }

    public JSONObject getData() {
        return data;
    }

    public void setData(JSONObject data) {
        this.data = data;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }


    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Block block = (Block) o;

        if (index != block.index) return false;
        if (!timetamp.equals(block.timetamp)) return false;
        if (!data.equals(block.data)) return false;
        if (!hash.equals(block.hash)) return false;
        return previousHash.equals(block.previousHash);

    }

    @Override
    public int hashCode() {
        int result = index;
        result = 31 * result + timetamp.hashCode();
        result = 31 * result + data.hashCode();
        result = 31 * result + hash.hashCode();
        result = 31 * result + previousHash.hashCode();
        return result;
    }
}
