/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mujistapha
 */
public class DbConnect {
    public Connection getConnection() throws SQLException{
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/papa?autoReconnet=true&useSSL=false", "root", "yourpassword");
        } catch (Exception ex) {
            System.out.println("Error when connecting to database: " + ex.getMessage());
        }
        return conn;
    }
}
