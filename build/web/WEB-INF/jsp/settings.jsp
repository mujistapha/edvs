<!DOCTYPE HTML>
<html>
    <head>
        <title>E-Document</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="../../style/style.css" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body text-color:#efefef>
          <table border="1" class= "container " id="hometable" style="">
            <tr>
                <td style = "background-color:#05386b;">
                    <a href=""><i class="fa fa-user icon"></i></a> <b style="margin-left:10px;color:#ffffff">Abdulrahman Musa</b>
                </td>
                <td style = "background-color:#5cdb95;">
                    <div class="row">
                        <div class = "col-xs-12 col-sm-12 col-md-6" >
                            <h1 style="margin-left:10px;color:#ffffff"><b>VERIFICATION PANEL<b></h1>
                                        </div>
                                        </div>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="background-color:#05386b">
                                                <br>
                                                <div class="menulist">
                                                    <a href="home.htm" class="btn menu" role="button">Home</a>
                                                    <a class="activemenu" href="home.html"><i class="fa fa-home icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="add.htm" class="btn menu" role="button">Add New</a>
                                                    <a href="add.htm"><i class="fa fa-plus icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="settings.htm" class="btn menu" role="button">Settings</a>
                                                    <a href="settings.htm"><i class="fa fa-cog icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="logout.htm" class="btn menu" role="button">Logout</a>
                                                    <a href="logout.htm"><i class="fa fa-sign-out icon"></i></a>
                                                </div>
                                                <br>
                                            </td>
                                            <td width="75%">		
                                                <table align="left">
                                                    <tr>
                                                        <td align= "center">
                                                            <a href=""><i style= "font-size: 200px; margin-left: 15px;" class="fa fa-user icon"></i></a> 
                                                        </td>
                                                        <td>
                                                            <h3 style="float:left">Abdulrahman Musa</h3>
                                                        </td><br>
                                                    </tr>
                                                    <tr>
                                                        <td align="">
                                                        <td style = "background-color:#5cdb95;" width="40%">
                                                            <form role="form" action="updatPassword.htm"  method="GET">
                                                                <h2>Change Password</h2>
                                                                <div class="form-group"> 
                                                                    <p style="text-align:center"><b>Enter Old Password:</b></p>
                                                                    <input class="form-control" style="width:80%; margin-left: 30px;" 
                                                                           placeholder="Enter Old Password:" name="old" type="password" required/>
                                                                </div>
                                                                <div class="form-group"> 
                                                                    <p style="text-align:center"><b>Enter New Password:</b> </p>
                                                                    <input class="form-control" style="width:80%; margin-left: 30px;" 
                                                                           placeholder="Enter New Password:" type="password" required/>
                                                                </div>
                                                                <div class="form-group"> 
                                                                    <p style="text-align:center"><b>Re-Enter New Password:</b> </p>
                                                                    <input class="form-control" style="width:80%; margin-left: 30px;" 
                                                                           name="new" placeholder="Re-Enter New Password:" type="password" required/>
                                                                </div>
                                                                <div>
                                                                    <input type="submit" id="changepassword" value="Change Password" 
                                                                           class="btn" style="background-color:#05386b;color:#ffffff;">
                                                                </div><br>
                                                            </form>
                                                            <div>
                                                                <br>
                                                                <p> 
                                                                <c:if test="${not empty errorMessage}">
                                                                    <c:out value="${errorMessage}"/>
                                                                </c:if>
                                                                </p>
                                                                <%
                                                                    request.getSession().setAttribute("errorMessage", null);
                                                                %>
                                                                </p>
                                                            </div>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        </table>

                                        </body>
                                        <footer>
                                            <p style="text-align:center">Copyright 2018. All Rights Reserved</p>
                                        </footer>
                                        </html>