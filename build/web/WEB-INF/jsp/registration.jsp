
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>E-Document</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="../../style/style.css" rel="stylesheet" />
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body text-color:#efefef>
        <table border="1" class= "container " id="hometable" style="">
            <tr>
                <td  style = "background-color:#5cdb95;">
                    <div class="row">
                        <div class = "col-xs-12 col-sm-12 col-md-6" >
                            <h2 style="margin-left:10px;color:#ffffff"><b>REGISTRATION<b></h2>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="75%">
                    <div class="row">
                        <form role="form" action="register.htm"  method="GET">
                            <div class="col-xs-12 col-sm-12 col-md-5" style="margin-left:10px">
                                <h3><b>Registration Body Information</b></h3>
                                <div class="form-group"> 
                                        <p><b>License ID:</b></p>
                                        <input class="form-control" name="license" placeholder="id number" type="text" required/>
                                </div>
                                <div class="form-group">
                                    <p><b>Body Name:</b></p>
                                    <input class="form-control" name="name" placeholder="name" type="text" required/>
                                </div>
                                <div class="form-group">
                                    <p><b>Address:</b></p>
                                    <input class="form-control" name="address"placeholder="Address" type="text" required/>
                                </div>
                                <div class="form-group">
                                    <p><b>Mobile Number:</b> </p>
                                    <input class="form-control" name="number" placeholder="Mobile Number" type="text" required/>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-5" style="float:right; margin-right: 15px;">
                                <h3 style="text-align:left"><b>Log Creation Details</b></h3>
                                <div class="">
                                    <p><b>User Name: </b></p>
                                    <input class="form-control" name="username" placeholder="User Name" type="text" required/>
                                </div>
                                <div class="form-group">
                                    <p><b>password:</b> </p>
                                    <input class="form-control"name="password" placeholder="Enter password" type="password" required/>
                                </div>
                                <div class="form-group">
                                    <p><b>Repeat password:</b> </p>
                                    <input class="form-control"id="" placeholder="Enter password" type="password" required/>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Register" class="btn submit"id="savedetail">
                                </div>
                                <div class="form-group">
                                    <p> 
                                    <c:if test="${not empty errorMessage}">
                                        <c:out value="${errorMessage}"/>
                                     </c:if>
                                    </p>
                                    <%
                                        request.getSession().setAttribute("errorMessage", null);
                                    %>
                                    </p>
                                    
                                </div>
                            </div>
                        </form>
                    </div>
                </td>
            </tr><br><br>
        </table>
            <footer>
                <p style="text-align:center">Copyright 2018. All Rights Reserved</p>
            </footer>
    </body>
    </html>