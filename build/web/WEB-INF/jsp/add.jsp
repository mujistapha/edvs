
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>E-Document</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="<c:out value="/style/style.css"/>" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body text-color:#efefef>
          <table border="1" class= "container " id="hometable" style="">
            <tr>
                <td style = "background-color:#05386b;">
                    <a href=""><i class="fa fa-user icon"></i></a> <b style="margin-left:10px;color:#ffffff">Abdulrahman Musa</b>
                </td>
                <td  style = "background-color:#5cdb95;">
                    <div class="row">
                        <div class = "col-xs-12 col-sm-12 col-md-6" >
                            <h2 style="margin-left:10px;color:#ffffff"><b>ADD NEW CERTIFICATE<b></h2>
                                        </div>
                           
                                        </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="background-color:#05386b">
                                                <br>
                                                <div class="menulist">
                                                    <a href="home.htm" class="btn menu" role="button">Home</a>
                                                    <a class="activemenu" href="home.html"><i class="fa fa-home icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="add.htm" class="btn menu" role="button">Add New</a>
                                                    <a href="add.htm"><i class="fa fa-plus icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="settings.htm" class="btn menu" role="button">Settings</a>
                                                    <a href="settings.htm"><i class="fa fa-cog icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="logout.htm" class="btn menu" role="button">Logout</a>
                                                    <a href="logout.htm"><i class="fa fa-sign-out icon"></i></a>
                                                </div>
                                                <br>
                                            </td>
                                            <td width="75%">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-12 col-md-5" style="margin-left:10px">
                                                        <h3><b>Certificate Holder Information</b></h3>
                                                        <form role="form" action="addCert.htm"  method="GET">
                                                            <div class="form-group"> 
                                                                <p><b>Full Name:</b> </p>
                                                                <input class="form-control" name="fullname" placeholder="Full Name" type="text" required/>
                                                            </div>
                                                            <div class="form-group">
                                                                <p><b>Date of Birth:</b> </p>
                                                                <input class="form-control" name="dob" placeholder="Date of Birth" type="date" required/>
                                                            </div>
                                                            <div class="form-group">
                                                                <p><b>Mobile Number:</b> </p>
                                                                <input class="form-control" name="mobileno" placeholder="Mobile Number" type="text" required/>
                                                            </div>
                                                            <div class="form-group">
                                                                <p><b>Email Address: </b></p>
                                                                <input class="form-control" name="email" placeholder="Email Address" type="text" required/>
                                                            </div>
                                                            


                                                    </div>
                                                    <div class="col-xs-12 col-sm-12 col-md-5" style="float:right; margin-right: 15px;">
                                                        <h3 style="text-align:left"><b>Certificate Details/ Identity</b></h3>

                                                        <div class="form-group"> 
                                                            <p><b>Title:</b> </p>
                                                            <input class="form-control" name="title" placeholder="Title of Award" type="text" required/>
                                                        </div>
                                                        <div class="form-group">
                                                            <p><b>Certificate ID:</b> </p>
                                                            <input class="form-control" name="certId" placeholder="Certificate ID" type="number" required/>
                                                        </div>
                                                    
                                                        <div class="form-group">
                                                            <p><b>Date Awarded: </b></p>
                                                            <input class="form-control" name="dateawarded" placeholder="Date Awarded" type="Date" required/>
                                                        </div>
                                                        
                                                        <div class="form-group">
                                                                <h3><b>Add Comment/ Status</b></h3>
                                                                <textarea cols="55" rows="5" name="comment">Add a comment...</textarea><br> <br>
                                                            </div>
                                                        <div class="form-group" >
                                                            <input type="submit" value="Save/ Confirm Details" class="btn submit" id="savedetail">
                                                        </div>
                                                        </form>
                                                    </div>

                                                </div>
                                        </tr>

                                        </table>
                                        </body>
                                        <footer>
                                            <p style="text-align:center">Copyright 2018. All Rights Reserved</p>
                                        </footer>
                                        </html>