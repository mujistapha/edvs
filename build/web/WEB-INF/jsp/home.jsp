<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>E-Document</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="<c:out value="/style/style.css" />" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body text-color:#efefef>
          <table border="1" class= "container " id="hometable" style="">
            <tr>
                <td style="background-color:#05386b">
                    <a href=""><i class="fa fa-user icon"></i></a> <b style="margin-left:10px;color:#ffffff">Abdulrahman Musa</b>
                </td>
                <td  style = "background-color:#5cdb95;">
                    <div class="row">
                        <div class = "col-xs-12 col-sm-12 col-md-6" >
                            <h1 style="margin-left:10px;color:#ffffff"><b>VERIFICATION PANEL<b></h1>
                                        </div>
                        <form role="form" action="search.htm"  method="GET">
                                        <div class = "col-xs-12 col-sm-12 col-md-6">
                                            <input type="text" name="searchid" placeholder="Search.." class="form-control" style= "margin-top: 30px; float: right; width: 50%; margin-right: 30px;">
                                            
                                        </div>
                            </form>
                                        </div>
                                        </td>
                                        </tr>
                                        <tr>
                                            <td width="25%" style="background-color:#05386b">
                                                <br>
                                                <div class="menulist">
                                                    <a href="home.htm" class="btn menu" role="button">Home</a>
                                                    <a class="activemenu" href="home.html"><i class="fa fa-home icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="add.htm" class="btn menu" role="button">Add New</a>
                                                    <a href="add.htm"><i class="fa fa-plus icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="settings.htm" class="btn menu" role="button">Settings</a>
                                                    <a href="settings.htm"><i class="fa fa-cog icon"></i></a>
                                                </div> <br>
                                                <div>
                                                    <a href="logout.htm" class="btn menu" role="button">Logout</a>
                                                    <a href="logout.htm"><i class="fa fa-sign-out icon"></i></a>
                                                </div>
                                                <br>
                                            </td>
                                            <td width="75%">		
                                                <table class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Certificate ID</th>
                                                            <th>Certificate Hash</th>
                                                            <th>Status of Certificate</th>
                                                            <th>Validity of Certificate</th>
                                                            <th>Date Added</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <c:forEach var="l" items="${ListOfCert}">
                                                            <tr>
                                                                <td>
                                                                    <a href="certdetail.htm?val=${l.getCertificateId()}"><c:out value="${l.getCertificateId()}"/></a></td>
                                                                <td>
                                                                    <a href="certdetail.htm?val=${l.getCertificateId()}"><c:out value="${l.getHash()}"/> </a>
                                                                </td>
                                                                <td><c:out value="${l.getStatus()}"/></td>
                                                                <td><c:out value="${l.getAwardDate()}"/></td>
                                                                <td><c:out value="${l.getDate()}"/></td>
                                                            </tr>
                                                        </c:forEach>

                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </table>

                                        </body>
                                        <footer>
                                            <p style="text-align:center">Copyright 2018. All Rights Reserved</p>
                                        </footer>
                                        </html>