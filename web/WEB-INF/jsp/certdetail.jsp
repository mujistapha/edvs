<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>E-Document</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="style.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    
    <body text-color:#efefef>
          <table border="1" class= "container " id="hometable" style="">
            <tr>
                <td style = "background-color:#05386b;">
                    <a href=""><i class=""></i></a> <b style="margin-left:10px;color:#ffffff"></b>
                </td>
                <td  style = "background-color:#5cdb95;">


                    </div>
                </td>
            </tr>
            <tr>
                <td width="75%">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-5" style="margin-left:20px">
                            <h3><b>Certificate Holder Information</b></h3>
                            <form>
                                <table style="width: 100%">
                                    <tr>
                                        <th style="width: 150px">Id Number:</th> 
                                        <th style="width: 150px">Name:</th>
                                        <th style="width: 150px">Status:</th>
                                    </tr>
                                    <c:forEach var="l" items="${certDetails}">
                                        <tr>
                                            <td style="width: 150px"><c:out value="${l.getcertificateID()}"/></td>
                                            <td style="width: 150px"><c:out value="${l.getholder()}"/></td>
                                            <td style="width: 150px"><c:out value="${l.getComment()}"/></td>
                                        </tr>
                                    </c:forEach>
                                    
                                </table>
                            </form>
                        </div>
                        
                    </div>
            </tr>
            
        </table>
          <br>
    </body>
    <footer>
        <p style="text-align:center; ">Copyright 2018. All Rights Reserved</p>
    </footer>
</html>