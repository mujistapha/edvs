<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
    <head>
        <title>E-Document</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link  href="<c:url value="/style/style.css" />" rel="stylesheet"  />

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    </head>
    <body text-color:#efefef>
          <div class= "container" style="padding-top:110px">
            <div class="jumbotron classjumbotron">
                <div class="row">
                    <div class = "col-xs-12 col-sm-12 col-md-6">
                        <h1 align="center">Welcome <br> To <br> E-Document <br> Verification<br> System</h1>
                    </div>
                    <div class = "col-xs-12 col-sm-12 col-md-6 formdiv">
                        <form role="form" action="login.htm"  method="post">
                            <div class="form-group"> 
                                <input class="form-control" id="username" name="Username" 
                                       placeholder="Username" type="text" required/>
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="password" name="Password" 
                                       placeholder="Password" type="password" required/>
                            </div>
                            <div>
                                <input type="submit" id="login" value="LOG IN"  class="btn submit">
                                <a href="forgotpassword.htm" id = "forgotpass">Forgot Password?</a>
                                <a href="registration.htm" id = "forgotpass"> Registration - </a>
                            </div>
                            <div>
                                <br>
                                <p> 
                                    <c:if test="${not empty errorMessage}">
                                        <c:out value="${errorMessage}"/>
                                    </c:if>
                                </p>
                                <%
                                    request.getSession().setAttribute("errorMessage", null);
                                %>
                                </p>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<footer>
    <p style="text-align:center">Copyright 2018. All Rights Reserved</p>
</footer>
</html>
